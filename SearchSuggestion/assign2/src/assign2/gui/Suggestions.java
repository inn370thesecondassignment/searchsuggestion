package assign2.gui;

import javax.swing.SwingUtilities;

import assign2.gui.Suggestions;

/**
 * This is the main class for the GUI
 * 
 * @author Mingzhuo LIU N8735751
 *
 */
public class Suggestions {
	
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new NGramGUI("Swing GUI Demo"));
	}
}
