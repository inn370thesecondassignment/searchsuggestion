package assign2.ngram;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

/**
 * @author Hunaynah Al Naamany N8820848
 * 
 */

public class NGramNodeTest {
	NGramNode contextNode, wordsNode;
	String context = "be or not to";
	String[] words = {"to", "be", "or", "not"};
	String[] predictions = {"be", "mention", "exceed", "say", "the"};
	Double[] probabilities = { 0.136059, 0.066563, 0.032759, 0.028824, 0.024524 };

	@Before 
	public void setUp() throws NGramException{		
		contextNode = new NGramNode(context, predictions, probabilities);
		wordsNode = new NGramNode(words, predictions, probabilities);
	}
	
	//Testing probabilities
	@Test
	public void getProbabilities() {
		assertArrayEquals(probabilities, contextNode.getProbabilities());
		}
	
	@Test
	public void setProbabilities() throws NGramException {
		Double[] Probabilities = {0.232, 0.3234};
		contextNode.setProbabilities(Probabilities);
		assertArrayEquals(Probabilities, contextNode.getProbabilities());
		}

		@Test 
		(expected = NGramException.class)
		public void setNullProbabilities() throws NGramException {
		Double[] Probabilities = null;
		contextNode.setProbabilities(Probabilities);
		}

		@Test 
		(expected = NGramException.class)
		public void setInvalidProbabilities() throws NGramException {
		Double[] Probabilities = {-2.0, 3.32};
		contextNode.setProbabilities(Probabilities);
		}

		//Testing Predictions
		@Test 
		public void getPredictions() {
		assertArrayEquals(predictions, contextNode.getPredictions());
		}

		@Test 
		public void setPredictions() throws NGramException {
		String[] Predictions = {"test", "predictions"};
		contextNode.setPredictions(Predictions);
		assertArrayEquals(Predictions, contextNode.getPredictions());
		}

		@Test 
		(expected = NGramException.class)
		public void setNullPredictions() throws NGramException {
		String[] Predictions = null;
		contextNode.setPredictions(Predictions);
		}

		@Test 
		(expected = NGramException.class)
		public void setEmptyPredictions() throws NGramException {
		String[] Predictions = {};
		contextNode.setPredictions(Predictions);
		}

		//Testing context
		@Test 
		public void getContextText() {
		assertEquals("be or not to", contextNode.getContext());	
		}
		
		@Test 
		public void getContext() {
		assertEquals(context, contextNode.getContext());	
		}

		@Test
		public void setStringContext() throws NGramException {
			contextNode.setContext("context");
			assertEquals("context", contextNode.getContext());
		}

		//Check*************************************************************
		@Test
		(expected = NGramException.class)
		public void setNullContext() throws NGramException {
			String Context = null;
			contextNode.setContext(Context);
			assertEquals(null, contextNode.getContext());
		}

		@Test
		public void setArrayContext() throws NGramException {
			String[] arrayString = {"Testing", "string", "array"};
			contextNode.setContext(arrayString);
			assertEquals("Testing string array", contextNode.getContext());
		}

		@Test
		(expected = NGramException.class)
		public void setEmptyArrayContext() throws NGramException {
			String[] arrayString = {};
			contextNode.setContext(arrayString);
			assertEquals(" ", contextNode.getContext());
		}
		
		//is using to assert ok?
		@Test
		(expected = NGramException.class)
		public void setNullArrayContext() throws NGramException {
			String[] arrayString = null;
			contextNode.setContext(arrayString);
			assertEquals(" ", contextNode.getContext());
			assertEquals(null, contextNode.getContext());
		}

		//Testing words context
		@Test
		(expected=NGramException.class)
		public void setNullWordsContext() throws NGramException {
			String[] Words = null;
			wordsNode.setContext(Words);
			assertEquals(null, wordsNode.getContext());
		}

		@Test
		(expected=NGramException.class)
		public void setEmptyWordsContext() throws NGramException {
			String[] Words = {};
			wordsNode.setContext(Words);
			assertEquals(" ", wordsNode.getContext());
		}

		@Test
		(expected=NGramException.class)
		public void setContextWithEmptyWord() throws NGramException {
			String[] Words = {"", "to", "be"};
			wordsNode.setContext(Words);
			assertEquals("to be", wordsNode.getContext());
		}
		
		@Test
		(expected=NGramException.class)
		public void setContextWithNullWord() throws NGramException {
			String[] Words = {null, "to", "be"};
			wordsNode.setContext(Words);
			assertEquals("to be", wordsNode.getContext());
		}

		//Check******************************
		//Testing words context and Predictions
		@Test
		(expected=NGramException.class)
		public void setWordsEmptyArrayPredictions() throws NGramException {
			String[] PredictionArray = {};
			wordsNode.setPredictions(PredictionArray);
			assertEquals("HI", wordsNode.getPredictions());
		}

		//Check******************************
		@Test
		(expected=NGramException.class)
		public void setWordsEmptyStringPredictions() throws NGramException {
			String[] predictionsString = {"", ""};
			wordsNode.setPredictions(predictionsString);
			assertEquals(" ", wordsNode.getPredictions());
		}

		//Testing words context and Probabilities
		@Test
		(expected=NGramException.class)
		public void setWordsNullProbabilities() throws NGramException {
			Double[] Probabilities = null;
			wordsNode.setProbabilities(Probabilities);
			assertEquals("", wordsNode.getProbabilities());
		}
		
		@Test
		public void setWordsProbabilities() throws NGramException {
			Double[] Probabilities = {0.9,0.5};
			wordsNode.setProbabilities(Probabilities);
		}
		
		@Test
		(expected=NGramException.class)
		public void setWordsWithNegativeProbabilities() throws NGramException {
			Double[] Probabilities = {-0.9,0.5};
			wordsNode.setProbabilities(Probabilities);
		}
		
		@Test
		(expected=NGramException.class)
		public void setWordsWithHighProbabilities() throws NGramException {
			Double[] Probabilities = {1.9,0.5};
			wordsNode.setProbabilities(Probabilities);
		}

		//need to add check if empty****************************
		@Test
		public void setWordsEmptyProbabilitiesSize() throws NGramException {
			Double[] Probabilities = {};
			wordsNode.setProbabilities(Probabilities);
		}

		@Test
		(expected=NGramException.class)
		public void setWordsZeroProbabilities() throws NGramException {
			Double[] Probabilities = {0.0};
			wordsNode.setProbabilities(Probabilities);
		}
}
