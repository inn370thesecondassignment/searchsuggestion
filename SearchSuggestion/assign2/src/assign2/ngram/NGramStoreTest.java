package assign2.ngram;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

/**
 * @author Hunaynah Al Naamany N8820848
 * 
 */

public class NGramStoreTest {

	NGramStore gStore;
	String context = "be or not to";
	String[] words = {"be", "or", "not", "to"};
	String[] predictions = {"be", "mention", "exceed", "say", "the"};
	Double[] probabilities = { 0.136059, 0.066563, 0.032759, 0.028824, 0.024524 };

	@Before
	public void setUp() throws NGramException {
		gStore = new NGramStore();
		gStore.getNGramsFromService("be or not to", 5);
	}

	@Test
	public void getNGramsFromServiceTest() throws NGramException {
		boolean test = gStore.getNGramsFromService("be or not", 5);
		assertTrue("testing true", test);		
	}

	@Test
	public void removeNGramTest() throws NGramException {
		NGramNode gNode = new NGramNode(context, predictions, probabilities);
		gStore.addNGram(gNode);
		gStore.removeNGram(context);
	}

	@Test
	public void getNGramTest() throws NGramException {
		NGramContainer gContainer = gStore.getNGram(context);
		assertEquals(gContainer.getContext(), context);
	}
	
	@Test
	public void getNGramNullContextTest() throws NGramException {
		context=null;
		gStore.getNGram(context);
		assertEquals(context, null);
		}
	
	@Test
	public void getNGramContextTest() throws NGramException {
		context="Test";
		gStore.getNGram(context);
		assertEquals(context, "Test");
		}

	//Need More Checking
	@Test
	public void addNGramTest() throws NGramException {
		NGramNode gNode1 = new NGramNode(context, predictions, probabilities);
		NGramNode gNode2 = new NGramNode(context, predictions, probabilities);
		gStore.addNGram(gNode1);
		gStore.addNGram(gNode2);
		assertEquals(context, context);
	}
	
	@Test
	public void addRemoveNGramTest() throws NGramException {
		NGramNode gNode1 = new NGramNode(context, predictions, probabilities);
		gStore.addNGram(gNode1);
		gStore.removeNGram(context);
		assertEquals(context, context);
	}
}
